//#define DEBUG
#define EXT_SIG

#define LED_ON_PIN PB0    // select the pin for switch on the LED
#define PIR_PIN PB1    // select the pin for switch on the LED
#define BUTTON_PIN PB2    // select the pin for switch on the LED

#ifdef EXT_SIG
#define EXT_SIG_PIN PB3    // select the pin for switch on the LED
#endif

#ifndef EXT_SIG
#define STATUS_LED_PIN PB3    // select the pin for switch on the LED
#endif

#define LIGHT_SENSOR_PIN A2 // select the input pin for the light sensor A2

#define ON_TIME 8000 //ms + 2000 default
#define LIGHT_ON_THRESHOLD 730
#define LIGHT_SWITCH_HYSTERESIS 30
