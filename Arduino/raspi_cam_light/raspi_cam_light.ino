#include "config.h"

#ifdef DEBUG
#include <EEPROM.h>
#endif


// GLOBAL VARIABLES
bool last_pir_state = LOW;
bool led_on_state = LOW;
uint32_t last_pir_event = 0;
bool last_button_state = HIGH;

void setup() {

#ifdef DEBUG
  // Check if there exist any OSCCAL value in EEPROM addr. 0
  // If not, run the oscillator tuner sketch first
  uint8_t cal = EEPROM.read(0);
  if (cal < 0x80)
    OSCCAL = cal;
  Serial.begin(115200);
  Serial.println("Start");
#endif

  pinMode(LED_ON_PIN, OUTPUT);
  analogWrite(LED_ON_PIN, LOW);

  pinMode(PIR_PIN, INPUT);
  pinMode(BUTTON_PIN, INPUT_PULLUP);

#ifdef EXT_SIG
  pinMode(EXT_SIG_PIN, INPUT);
#endif

#ifndef EXT_SIG
  pinMode(STATUS_LED_PIN, OUTPUT);
  digitalWrite(STATUS_LED_PIN, LOW);
#endif

  pinMode(LIGHT_SENSOR_PIN, INPUT);

}

long my_map(long x, long in_min, long in_max, long out_min, long out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void loop() {
  int light_sensor_value = analogRead(LIGHT_SENSOR_PIN);
  int converted_value = my_map(light_sensor_value, 0, 1023, 255, 0);
  bool pir_state = digitalRead(PIR_PIN);
  bool button_state = digitalRead(BUTTON_PIN);

  //rising edge detection of PIR Sensor
  if (pir_state == HIGH && last_pir_state == LOW) {
    last_pir_state = HIGH;
  }
  
  //falling edge detection
  else if (pir_state == LOW && last_pir_state == HIGH) {
    last_pir_state = LOW;
  }

  if (pir_state == HIGH) {
    //digitalWrite(STATUS_LED_PIN, HIGH);
    last_pir_event = millis();
  }
  else {
    //digitalWrite(STATUS_LED_PIN, LOW);
  }
  
  if (light_sensor_value <= LIGHT_ON_THRESHOLD - LIGHT_SWITCH_HYSTERESIS && millis() - last_pir_event <= ON_TIME){
    led_on_state = HIGH;
  }
  else{
    led_on_state = LOW;
  }

  digitalWrite(LED_ON_PIN, led_on_state);
  //analogWrite(LED_ON_PIN, converted_value);


  //falling edge detection of button
  if (button_state == LOW && last_button_state == HIGH) {
    last_button_state = LOW;
    for (int i=0; i<=5; i++){
      digitalWrite(STATUS_LED_PIN, HIGH);
      delay(100);
      digitalWrite(STATUS_LED_PIN, LOW);
      delay(100);
    }
  }
  else if (button_state == HIGH && last_button_state == LOW){
    last_button_state = HIGH;
  }
  
    
  #ifdef DEBUG
  Serial.print("Light Sensor Value: ");
  Serial.print(light_sensor_value);
  Serial.print(" | Converted Value: ");
  Serial.println(converted_value);
  #endif
  
  delay(1);
}
